package patterns.singleton;

public class NewsService {
    private static NewsService newsService;
    private static String logFile = "This is News. \n\n";

    public static synchronized NewsService getNewsService() {
        if (newsService == null) {
            newsService = new NewsService();
        }
        return newsService;
    }

    private NewsService() {

    }

    public void addNewsInfo(String logInfo) {
        logFile += logInfo + "\n";
    }

    public void showNews() {
        System.out.println(logFile);
    }
}
