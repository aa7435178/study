package executorService;

import java.time.LocalDateTime;

public class Task implements  Runnable{
    @Override
    public void run() {
        try {
            System.out.println("Hello World!"+ LocalDateTime.now());
            Thread.sleep(5000);
            System.out.println("Goodbye World!"+ LocalDateTime.now());


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
