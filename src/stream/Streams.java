package stream;


import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class Streams {

    public static void main(String[] args) {
        int number = new Random().nextInt(100);

        List<News> listNews = List.of(new News(number, "Змея №" + number),
                new News(22, "Кот №" + number),
                new News(22, "Собака №" + number));
        Map<Integer, List<News>> map = listNews
                .stream()
                .filter(x -> x.getId() % 2 == 0)
                .map(news -> {
                    news.setId(news.getId() * 10);
                    return news;
                })
                .collect(Collectors.groupingBy(News::getId));
        System.out.println(map);
    }
}
