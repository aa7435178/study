package functionalInterface;

@java.lang.FunctionalInterface
interface MakeMeRich {
    int calculate(int dollar);
}

public class FunctionalInterface {
    public static void main(String[] args) {
        int cash = 1;
        int card = 25;

        MakeMeRich aMilliDollarMan = (int dollar) -> dollar * 1000000;

        int myCash = aMilliDollarMan.calculate(cash);
        int myCard = aMilliDollarMan.calculate(card);

        System.out.println("Кто только что слал богаче на " + myCard + myCash + " баксов?");

    }

}
