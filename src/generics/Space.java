package generics;

public class Space {
    public static void main(String[] args) {
        SpaceBodyName<String> body = new SpaceBodyName<>();
        body.setName("Earth", "Moon");

        System.out.println(body.getName(SpaceBodies.PLANET));
        System.out.println(body.getName(SpaceBodies.SPUTNIK));

    }

    enum SpaceBodies {
        PLANET,
        SPUTNIK;
    }

    interface SpaceInterface<BODY> {
        void setName(BODY planet, BODY sputnik);

        BODY getName(BODY planet, BODY sputnik);
    }

    static class SpaceBodyName<BODY_NAME> implements SpaceInterface<BODY_NAME> {
        private BODY_NAME planet, sputnik;

        public SpaceBodyName(BODY_NAME planet, BODY_NAME sputnik) {

        }

        public SpaceBodyName() {

        }

        public void setName(BODY_NAME planet, BODY_NAME sputnik) {
            this.planet = planet;
            this.sputnik = sputnik;
        }

        @Override
        public BODY_NAME getName(BODY_NAME planet, BODY_NAME sputnik) {
            return null;
        }

        public BODY_NAME getName(SpaceBodies spaceBodies) {
            return (spaceBodies == SpaceBodies.PLANET) ? planet : sputnik;
        }
    }
}
