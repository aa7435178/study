package generics;

public class LetterTwo {
private Object object;

    public LetterTwo(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

}
