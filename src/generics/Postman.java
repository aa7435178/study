package generics;

public class Postman {
    public static void main(String[] args) {
        Letter<String> pismo = new Letter<>("Письмо  Юрию!", 231555);
        String text = pismo.getT();
        String index = String.valueOf(pismo.getIndex());
        System.out.println(text + " Индекс письма: " + index);
        Letter<NewsExample> letterNewsExample = new Letter<>(new NewsExample("ТЫ АБЬЮЗЕР!"), 111);
        String coco = letterNewsExample.getT().getTitle();
        System.out.println(coco);

        LetterTwo letterTwo = new LetterTwo("Здрасти!");
        //LetterTwo letterTwo = new LetterTwo(2323);
        String hello = letterTwo.getObject().toString();
        System.out.println(hello);
        Letter<NewsExample> one = new Letter<>(new NewsExample("ляля"), 1);
        LetterTwo two = new LetterTwo((new NewsExample("object")));
        System.out.println(one.getT().getTitle());
        /////////////////////////////////////////////

method(23);
    }

    ///обобщенный метод
    static <T> void method (T t){
        System.out.println(t.toString());
    }
}
