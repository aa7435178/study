package generics;

public class Letter<T> {
   private T t;
   private Integer index;

    public Letter(T t, Integer index) {
        this.t = t;
        this.index = index;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
